<?php

namespace App\Http\Controllers;

use App\Models\Windaliani_nim;
use App\Http\Requests\StoreWindaliani_nimRequest;
use App\Http\Requests\UpdateWindaliani_nimRequest;
use Illuminate\Http\Request;

class WindalianiNimController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
         $mahasiswas = Windaliani_nim::get();

        return view("index", ["mahasiswas" => $mahasiswas]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view("create");
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $mahasiswa = new Windaliani_nim;
        $mahasiswa->nim = $request->nim;
        $mahasiswa->nama = $request->nama;
        $mahasiswa->save();

        return redirect()->route('mahasiswa.index');
    }

    /**
     * Display the specified resource.
     */
    public function show(Windaliani_nim $mahasiswa)
    {
        return "tampilan untuk detail data mahasiswa";
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Windaliani_nim $mahasiswa)
    {
        return view("edit", [
            "mahasiswa" => $mahasiswa
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Windaliani_nim $mahasiswa)
    {
        // aksi untuk mengupdate data mahasiswa
        $mahasiswa->nim = $request->nim;
        $mahasiswa->nama = $request->nama;
        $mahasiswa->save();

        return redirect()->route('mahasiswa.index');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Windaliani_nim $mahasiswa)
    {
        $mahasiswa->delete();

        return redirect()->route('mahasiswa.index');
    }
}
