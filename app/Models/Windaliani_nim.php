<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Windaliani_nim extends Model
{
    use HasFactory;
    protected $table = "Windaliani_nims";
    protected $primaryKey = 'id';
    protected $fillable =
    [
        'nim',
        'nama'
    ];
}
